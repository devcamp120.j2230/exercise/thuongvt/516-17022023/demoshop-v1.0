import "bootstrap/dist/css/bootstrap.min.css"
import Body from "./component/Body/body";
import Footer from "./component/Footer/footer";
import Header from "./component/header/header";

function App() {
  return (
    <div>
      {/* header */}
      <Header/>
      {/* Body */}
      <Body/>
      {/* footer */}
      <Footer/>
    </div>
  );
}

export default App;
