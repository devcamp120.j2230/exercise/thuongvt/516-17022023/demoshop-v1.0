import { Component } from "react";

class Description extends Component {
    render() {
        return (
            <>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3><b>Product List</b></h3>
                                <p> Showing 1 - 9 of 24 products</p>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Description