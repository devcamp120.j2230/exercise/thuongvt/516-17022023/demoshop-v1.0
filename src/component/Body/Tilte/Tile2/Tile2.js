import { Component } from "react";

class Tilte2 extends Component {
    render(){
        return(
            <>
            <div class="col-6 col-sm-4 " style={{position: "relative",paddingRight: "15px",paddingLeft: "15px"}}>
                    <div class="card mb-3" style={{Width: "18rem ", padding:"20px"}}>
                        <div class="card-header">
                            <h5 class="card-title" style={{fontSize:"1.25rem", textAlign: "center"}}>
                                <a href={{}}>Bakery</a>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="card-img" style={{minHeight:"12rem", textAlign: "center!important"}}>
                                <a href={{}}>
                                    <img src="http://www.loop54.com/hubfs/demo_images/cinnamonroll.jpg" alt="Bakery" 
                                    style={{width:"200px",display:" block", marginLeft: "auto", marginRight: "auto"}}/>
                                </a>
                                <p class="card-text description" style={{textAlign: "center",width:"fit-content;"}}>
                                A cinnamon roll (also cinnamon bun, cinnamon swirl, cinnamon Danish and cinnamon snail) is a sweet roll served commonly in Northern Europe and North America. In North America its common use is for breakfast. Its main ingredients are flour, cinnamon, sugar, and butter, which provide a robust and sweet flavor. In some places it is eaten as a breakfast food and is often served with cream cheese or icing.</p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Category:</b>
                                    Bakery
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Made by:</b>
                                    Awesome bakery
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Organic:</b>
                                    No
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Price: </b>
                                    $
                                    5
                                </p>
                                <div class="add-tilte" style={{textAlign: "center"}}>
                                    <button type="button" class="btn btn-primary">
                                        Add to Cart
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Tilte2