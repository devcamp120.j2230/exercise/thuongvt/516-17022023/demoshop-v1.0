import { Component } from "react";

class Tilte5 extends Component {
    render(){
        return(
            <>
            <div class="col-6 col-sm-4 " style={{position: "relative",paddingRight: "15px",paddingLeft: "15px"}}>
                    <div class="card mb-3" style={{Width: "18rem ", padding:"20px"}}>
                        <div class="card-header">
                            <h5 class="card-title" style={{fontSize:"1.25rem", textAlign: "center"}}>
                                <a href={{}}>Apple</a>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="card-img" style={{minHeight:"12rem", textAlign: "center!important"}}>
                                <a href={{}}>
                                    <img src="http://www.loop54.com/hubfs/demo_images/apple.jpg" alt="Bakery" 
                                    style={{width:"200px",display:" block", marginLeft: "auto", marginRight: "auto"}}/>
                                </a>
                                <p class="card-text description" style={{textAlign: "center",width:"fit-content;"}}>
                                The apple tree (Malus domestica) is a deciduous tree in the rose family best known for its sweet, pomaceous fruit, the apple. It is cultivated worldwide as a fruit tree, and is the most widely grown species in the genus Malus. The tree originated in Central Asia, where its wild ancestor, Malus sieversii, is still found today. Apples have been grown for thousands of years in Asia and Europe, and were brought to North America by European colonists. Apples have religious and mythological significance in many cultures, including Norse, Greek and European Christian traditions.
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Category:</b>
                                    Fruit
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Made by:</b>
                                    Fruits n Veggies
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Organic:</b>
                                    No
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Price: </b>
                                    $
                                    6
                                </p>
                                <div class="add-tilte" style={{textAlign: "center"}}>
                                    <button type="button" class="btn btn-primary">
                                        Add to Cart
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Tilte5