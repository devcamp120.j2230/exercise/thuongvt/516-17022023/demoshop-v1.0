import { Component } from "react";

class Tilte1 extends Component {
    render(){
        return(
            <>
            <div class="col-6 col-sm-4 " style={{position: "relative",paddingRight: "15px",paddingLeft: "15px"}}>
                    <div class="card mb-3" style={{Width: "18rem ",padding:"20px"}}>
                        <div class="card-header">
                            <h5 class="card-title" style={{fontSize:"1.25rem", textAlign: "center"}}>
                                <a href={{}}>Banana</a>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="card-img" style={{minHeight:"12rem", textAlign: "center!important"}}>
                                <a href={{}}>
                                    <img src="http://www.loop54.com/hubfs/demo_images/banana.jpg" alt="banana" 
                                    style={{width:"200px",display:" block", marginLeft: "auto", marginRight: "auto"}}/>
                                </a>
                                <p class="card-text description" style={{textAlign: "center",width:"fit-content;"}}>
                                The banana is an edible fruit, botanically a berry, produced by several kinds of large herbaceous flowering plants in the genus Musa. In some countries, bananas used for cooking may be called plantains. The fruit is variable in size, color and firmness, but is usually elongated and curved, with soft flesh rich in starch covered with a rind which may be green, yellow, red, purple, or brown when ripe. The fruits grow in clusters hanging from the top of the plant. Almost all modern edible parthenocarpic (seedless) bananas come from two wild species - Musa acuminata and Musa balbisiana. The scientific names of most cultivated bananas are Musa acuminata, Musa balbisiana, and Musa x paradisiaca for the hybrid Musa acuminata x M. balbisiana, depending on their genomic constitution. The old scientific name Musa sapientum is no longer used. It is also yellow.
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Category:</b>
                                    Fruit
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Made by:</b>
                                    The banana company
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Organic:</b>
                                    Yes
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Price: </b>
                                    $
                                    7
                                </p>
                                <div class="add-tilte" style={{textAlign: "center"}}>
                                    <button type="button" class="btn btn-primary">
                                        Add to Cart
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Tilte1