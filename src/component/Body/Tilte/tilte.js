import { Component } from "react";
import Tilte1 from "./Tile1/Tilte1";
import Tilte2 from "./Tile2/Tile2";
import Tilte3 from "./Tile3/Tilte3";
import Tilte4 from "./Tile4/Tilte4";
import Tilte5 from "./Tile5/Tilte5";
import Tilte6 from "./Tile6/Tilte6";
import Tilte7 from "./Tile7/Tilte7";
import Tilte8 from "./Tile8/Tilte8";
import Tilte9 from "./Tile9/Tilte";

class Tilte extends Component {

    render(){
        return(
           <div className="div-tilte" style={{display:"flex", justifyContent: "center",flexShrink:"1",flexWrap: "wrap"}}>
            <Tilte1/>
            <Tilte2/>
            <Tilte3/>
            <Tilte4/>
            <Tilte5/>
            <Tilte6/>
            <Tilte7/>
            <Tilte8/>
            <Tilte9/>
           </div>
        )
    }
}

export default Tilte
