import { Component } from "react";

class Tilte4 extends Component {
    render(){
        return(
            <>
            <div class="col-6 col-sm-4 " style={{position: "relative",paddingRight: "15px",paddingLeft: "15px"}}>
                    <div class="card mb-3" style={{Width: "18rem ", padding:"20px"}}>
                        <div class="card-header">
                            <h5 class="card-title" style={{fontSize:"1.25rem", textAlign: "center"}}>
                                <a href={{}}>Milk</a>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="card-img" style={{minHeight:"12rem", textAlign: "center!important"}}>
                                <a href={{}}>
                                    <img src="http://www.loop54.com/hubfs/demo_images/mincedmeat.jpg" alt="meat" 
                                    style={{width:"200px",display:" block", marginLeft: "auto", marginRight: "auto"}}/>
                                </a>
                                <p class="card-text description" style={{textAlign: "center",width:"fit-content;"}}>
                                Milk is a white liquid produced by the mammary glands of mammals. It is the primary source of nutrition for young mammals before they are able to digest other types of food. Early-lactation milk contains colostrum, which carries the mother's antibodies to its young and can reduce the risk of many diseases. Milk contains many other nutrients and the carbohydrate lactose.
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Category:</b>
                                    Milk
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Made by:</b>
                                    Early
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Organic:</b>
                                    No
                                </p>
                                <p class="card-text" style={{textAlign: "left;"}}>
                                    <b>Price: </b>
                                    $
                                    10
                                </p>
                                <div class="add-tilte" style={{textAlign: "center"}}>
                                    <button type="button" class="btn btn-primary">
                                        Add to Cart
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Tilte4