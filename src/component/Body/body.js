import { Component } from "react";
import Description from "./Description/description";
import Tilte from "./Tilte/tilte";

class Body extends Component {
    render(){
        return(
            <>
            <div className="container">
               <Description/>
               <Tilte/>
            </div>
            </>
        )
    }
}
export default Body