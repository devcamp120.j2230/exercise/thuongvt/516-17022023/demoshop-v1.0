import { Component } from "react";

class Footer extends Component{
    render(){
        return(
            <>
            <footer class="page-footer font-small pt-2">
            <div class="container-fluid text-light bg-dark">
                <div class="row">
                    <div class="col-md-6 mt-md-6 mt-3">
                        <div class="container">
                            <p style={{text:"center", height: "fit-content"}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua.</p>
                            <p> © 2018 . All Rights Reserved</p>
                        </div>
                    </div>
                    <hr class="clearfix w-100 d-md-none pb-3"/>
                    <div class="col-md-3 mb-md-0 mb-3" style={{text:"left"}}>
                        <h5>Contacts</h5>
                        <ul class="list-unstyled ">
                            <li>
                                <a href={{}}>Address:</a>
                                <p>Kolkata, West Bengal, India</p>
                            </li>
                            <li>
                                <a href={{}}>email:</a>
                                <p>thuongcuu@gmail.com</p>
                            </li>
                            <li>
                                <a href={{}}>phones:</a>
                                <p>0989753899</p>
                            </li>
                        </ul>

                    </div>
                    <div class="col-md-3 mb-md-0 mb-3">
                        <h5 style={{text:"left"}}>Links</h5>
                        <ul class="list-unstyled">
                            <li>
                                <p href="#!">About</p>
                            </li>
                            <li>
                                <p href="#!">Projects</p>
                            </li>
                            <li>
                                <p href="#!">Blog</p>
                            </li>
                            <li>
                                <p href="#!">Contacts</p>
                            </li>
                            <li>
                                <p href="#!">Pricing</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright text-center py-3 bg-dark text-light">
                <div class="row">
                    <div class="col-md-3">FAECBOOK</div>
                    <div class="col-md-3">INSTAGRAM</div>
                    <div class="col-md-3">TWITTER</div>
                    <div class="col-md-3">GOOGLE</div>
                </div>
            </div>
        </footer>
            </>
        )
    }
}

export default Footer